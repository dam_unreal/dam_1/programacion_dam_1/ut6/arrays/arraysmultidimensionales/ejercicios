package arraysMultidimensionales;

public class Ejercicio04 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		int tabla[][] = new int[3][3];
				
		//DAR VALORES
		for(int f=0;f<tabla.length;f++)
			for(int c=0;c<tabla[f].length;c++){
				if(c==f)
					tabla[f][c]=1;
				else
					tabla[f][c]=0;
			}
				
				
		for(int f=0;f<tabla.length;f++){
			for(int c=0;c<tabla[f].length;c++)
				System.out.print(tabla[f][c]+" ");
				System.out.println();
		}

	}

}
