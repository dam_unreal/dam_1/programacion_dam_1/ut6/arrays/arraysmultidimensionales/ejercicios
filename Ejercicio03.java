package arraysMultidimensionales;

public class Ejercicio03 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		int tabla[][] = new int[7][7];
		
		//DAR VALORES
		for(int f=0;f<tabla.length;f++)
			for(int c=0;c<tabla[f].length;c++)
				tabla[f][c] = f+c;
		
		for(int f=0;f<tabla.length;f++){
			for(int c=0;c<tabla[f].length;c++)
				System.out.print(tabla[f][c]+" ");
			System.out.println();
		}		
		
	}

}
