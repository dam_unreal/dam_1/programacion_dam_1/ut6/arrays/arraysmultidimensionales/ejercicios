package arraysMultidimensionales;

public class ArraysIrregulares {

	public static void main(String[] args) {
		
		//METODO A
		int a[][] = {{1,2,3},{4,5},{6,7,8,9,10}};
		
		//METODO B
		int b[][] = new int [4][];
		b[0] = new int[3];
		b[1] = new int[2];
		b[2] = new int[7];
		b[3] = new int[4];
	}

}
