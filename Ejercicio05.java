package arraysMultidimensionales;

public class Ejercicio05 {

	public static void main(String[] args) {

		//INICIALIZAR
		int tabla[][] = new int[4][3];
		int fila[] = new int[4];
				
		//DAR VALORES
		for(int f=0;f<tabla.length;f++){
			for(int c=0;c<tabla[f].length;c++){
				tabla[f][c] = (int)(Math.random()*9)+1;
			}
			fila[f] = tabla[f][0]+tabla[f][1]+tabla[f][2];
		}
			
				
				
		for(int f=0;f<tabla.length;f++){
			for(int c=0;c<tabla[f].length;c++)
				System.out.print(tabla[f][c]+" ");
				System.out.println();
		}
		
		System.out.println();
		System.out.println("Fila 1: "+fila[0]);
		System.out.println("Fila 2: "+fila[1]);
		System.out.println("Fila 3: "+fila[2]);
		System.out.println("Fila 4: "+fila[3]);
		
	}

}