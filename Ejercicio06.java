package arraysMultidimensionales;

public class Ejercicio06 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		int tabla[][] = new int[4][3];
		int columna[] = new int[3];
				
		//DAR VALORES
		for(int c=0;c<3;c++){
			for(int f=0;f<4;f++){
				tabla[f][c] = (int)(Math.random()*9)+1;
			}
			columna[c] = tabla[0][c]+tabla[1][c]+tabla[2][c]+tabla[3][c];
		}
			
				
				
		for(int f=0;f<tabla.length;f++){
			for(int c=0;c<tabla[f].length;c++)
				System.out.print(tabla[f][c]+" ");
				System.out.println();
		}
		
		System.out.println();
		System.out.println("Columna 1: "+columna[0]);
		System.out.println("Columna 2: "+columna[1]);
		System.out.println("Columna 3: "+columna[2]);

	}

}
