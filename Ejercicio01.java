package arraysMultidimensionales;

import java.util.Arrays;

public class Ejercicio01 {

	public static void main(String[] args) {
		
		//APARTADO A, DECLARACION MANUAL
		int tabla[][] = {
				{1,2,3,4},
				{5,6,7,8},
				{9,10,11,12}
		};
		
		for(int fila=0;fila<tabla.length;fila++){
			for(int columna=0;columna<tabla[fila].length;columna++)
				System.out.print(tabla[fila][columna]+" ");
			System.out.println();
		}
		
		System.out.println("##################################");
		
		
		int tamañoFila = tabla.length, tamañoColumna = tabla[0].length;
		
		
		for(int c=0;c<tamañoColumna;c++){
			for(int f=0;f<tamañoFila;f++)
				System.out.print(tabla[f][c]+" ");
			System.out.println();
		}
		
		System.out.println("##################################");
		
		//APARTADO B
		int tabla2[][] = new int[3][4];
		for(int fila=0;fila<tabla2.length;fila++)
			for(int columna=0;columna<tabla2[fila].length;columna++)
				tabla2[fila][columna] = (int)(Math.random()*9)+1;
		
		for(int fila=0;fila<tabla2.length;fila++){
			for(int columna=0;columna<tabla2[fila].length;columna++)
				System.out.print(tabla2[fila][columna]+" ");
			System.out.println();
		}
		
		System.out.println("##################################");
		
	}

}